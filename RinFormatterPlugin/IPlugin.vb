﻿Public Interface IPlugin

    ''' <summary>
    ''' Gets the plugin name.
    ''' </summary>
    ReadOnly Property getPluginName As String

    ''' <summary>
    ''' Gets the plugin's unique id.
    ''' </summary>
    ReadOnly Property getPluginId As String

    ''' <summary>
    ''' Gets the plugin author.
    ''' </summary>
    ReadOnly Property getPluginAuthor As String

    ''' <summary>
    ''' Gets the plugin version.
    ''' </summary>
    ReadOnly Property getPluginVersion As String

    ''' <summary>
    ''' Determine if the plugin converts the text line by line.
    ''' </summary>
    ''' <returns><c>True</c> if the texts are converted line by line; otherwise <c>False</c>.</returns>
    ReadOnly Property isLineByLine As Boolean

    ''' <summary>
    ''' Determine if the plugin has a configuration dialogue.
    ''' </summary>
    ''' <returns><c>True</c> if the configuration dialogue is accessible; otherwise <c>False</c>.</returns>
    ''' <remarks>The configuration dialogue will be called by <see cref="IPlugin.showDialogue" /> if accessible.</remarks>
    ReadOnly Property hasDialogue As Boolean

    ''' <summary>
    ''' Initialize the plugin by giving it settings.
    ''' </summary>
    ''' <remarks>It is recommended to fetch the settings and save them to fields for other methods.</remarks>
    Sub initialize(ByVal settings As Setting)

    ''' <summary>
    ''' Gets the current settings.
    ''' </summary>
    ''' <returns>The current settings as <see cref="Setting" />.</returns>
    Function getSettings() As Setting

    ''' <summary>
    ''' Converts the whole texts passed in.
    ''' </summary>
    ''' <returns>The whole texts.</returns>
    Function convert(ByVal lines As String()) As String()

    ''' <summary>
    ''' Converts the <see cref="String" /> passed in.
    ''' </summary>
    ''' <returns>The converted <see cref="String" />.</returns>
    Function convert(ByVal line As String) As String

    ''' <summary>
    ''' Shows the configuration dialogue this plugin has.
    ''' </summary>
    ''' <remarks>This will only be called if <see cref="IPlugin.hasDialogue" /> is <c>True</c>.</remarks>
    Sub showDialogue()

End Interface
