﻿Public Class Configuration

    Private _settings As Dictionary(Of String, RinFormatterPlugin.Setting)

    Public Sub New()
        _settings = New Dictionary(Of String, RinFormatterPlugin.Setting)
    End Sub

    Public Sub setSettings(ByVal pluginId As String, ByVal settings As RinFormatterPlugin.Setting)
        If (_settings.ContainsKey(pluginId)) Then
            _settings.Item(pluginId) = settings
        Else
            _settings.Add(pluginId, settings)
        End If
    End Sub

    Public Function getSettings(ByVal pluginId As String) As RinFormatterPlugin.Setting
        Return IIf(_settings.ContainsKey(pluginId), _settings.Item(pluginId), Nothing)
    End Function

    Public Sub destroyUnloaded(ByVal plugins As ArrayList)
        For Each pluginId As String In _settings.Keys
            If (plugins.Contains(pluginId)) Then
                _settings.Remove(pluginId)
            End If
        Next
    End Sub

End Class
