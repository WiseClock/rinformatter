﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RinFormatter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.menuBar = New System.Windows.Forms.MainMenu(Me.components)
        Me.menuPlugin = New System.Windows.Forms.MenuItem()
        Me.menuAbout = New System.Windows.Forms.MenuItem()
        Me.menuToolBar = New System.Windows.Forms.ToolBar()
        Me.toolOpen = New System.Windows.Forms.ToolBarButton()
        Me.toolSave = New System.Windows.Forms.ToolBarButton()
        Me.toolConvert = New System.Windows.Forms.ToolBarButton()
        Me.toolConfig = New System.Windows.Forms.ToolBarButton()
        Me.contentBox = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'menuBar
        '
        Me.menuBar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuPlugin, Me.menuAbout})
        '
        'menuPlugin
        '
        Me.menuPlugin.Index = 0
        Me.menuPlugin.Text = "menuPlugin"
        '
        'menuAbout
        '
        Me.menuAbout.Index = 1
        Me.menuAbout.Text = "menuAbout"
        '
        'menuToolBar
        '
        Me.menuToolBar.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.toolOpen, Me.toolSave, Me.toolConvert, Me.toolConfig})
        Me.menuToolBar.DropDownArrows = True
        Me.menuToolBar.Location = New System.Drawing.Point(0, 0)
        Me.menuToolBar.Name = "menuToolBar"
        Me.menuToolBar.ShowToolTips = True
        Me.menuToolBar.Size = New System.Drawing.Size(284, 42)
        Me.menuToolBar.TabIndex = 0
        '
        'toolOpen
        '
        Me.toolOpen.Name = "toolOpen"
        Me.toolOpen.Text = "Open"
        '
        'toolSave
        '
        Me.toolSave.Name = "toolSave"
        Me.toolSave.Text = "Save"
        '
        'toolConvert
        '
        Me.toolConvert.Name = "toolConvert"
        Me.toolConvert.Text = "Convert"
        '
        'toolConfig
        '
        Me.toolConfig.Name = "toolConfig"
        Me.toolConfig.Text = "Config"
        '
        'contentBox
        '
        Me.contentBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.contentBox.Location = New System.Drawing.Point(0, 42)
        Me.contentBox.Multiline = True
        Me.contentBox.Name = "contentBox"
        Me.contentBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.contentBox.Size = New System.Drawing.Size(284, 220)
        Me.contentBox.TabIndex = 1
        '
        'RinFormatter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.contentBox)
        Me.Controls.Add(Me.menuToolBar)
        Me.Menu = Me.menuBar
        Me.Name = "RinFormatter"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents menuBar As System.Windows.Forms.MainMenu
    Friend WithEvents menuPlugin As System.Windows.Forms.MenuItem
    Friend WithEvents menuAbout As System.Windows.Forms.MenuItem
    Friend WithEvents menuToolBar As System.Windows.Forms.ToolBar
    Friend WithEvents toolOpen As System.Windows.Forms.ToolBarButton
    Friend WithEvents toolSave As System.Windows.Forms.ToolBarButton
    Friend WithEvents toolConvert As System.Windows.Forms.ToolBarButton
    Friend WithEvents toolConfig As System.Windows.Forms.ToolBarButton
    Friend WithEvents contentBox As System.Windows.Forms.TextBox

End Class
