﻿Imports RinFormatterPlugin
Public Class RinFormatter

    Private loadedPlugins As List(Of IPlugin) = New List(Of IPlugin)
    Private configs As Configuration

    Private Sub RinFormatter_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'create configuration
        configs = New Configuration()
        'TODO: Check if the configuration file exists.

        'load plugins
        loadedPlugins.Add(New Indentation())
        'TODO: Load from plugin folder also.

        'loaded plugin ids
        Dim loadedPluginIds As ArrayList = New ArrayList

        'for each plugin
        For Each pl As IPlugin In loadedPlugins
            'put its id into the list
            loadedPluginIds.Add(pl.getPluginId)

            Dim mi As MenuItem = New MenuItem(pl.getPluginName)
            If pl.hasDialogue Then
                Dim plSettings As Setting = IIf(configs.getSettings(pl.getPluginId) Is Nothing, New Setting, configs.getSettings(pl.getPluginId))
                'initialize the plugin
                pl.initialize(plSettings)
                Dim smi As MenuItem = New MenuItem("config")
                AddHandler smi.Click, AddressOf pl.showDialogue
                mi.MenuItems.Add(smi)
            End If
            menuPlugin.MenuItems.Add(mi)
        Next
        'destroy the unloaded plugins
        configs.destroyUnloaded(loadedPluginIds)
        loadedPluginIds = Nothing
        'TODO: Save the configuration.
    End Sub

    Private Sub menuToolBar_ButtonClick(sender As Object, e As ToolBarButtonClickEventArgs) Handles menuToolBar.ButtonClick
        Select Case menuToolBar.Buttons.IndexOf(e.Button)
            Case menuToolBar.Buttons.IndexOf(toolConvert)
                If contentBox.Text.Length > 0 Then
                    Dim contents As String() = contentBox.Text.Split(vbNewLine)
                    ''convert
                    For i As Integer = 0 To contents.Length - 1
                        For Each pl As IPlugin In loadedPlugins
                            If pl.isLineByLine Then
                                'convert line by line
                                contents(i) = pl.convert(contents(i))
                            Else
                                'convert the whole text
                                contents = pl.convert(contents)
                            End If
                        Next
                    Next
                    ''print
                    contentBox.Text = ""
                    For Each line As String In contents
                        contentBox.AppendText(line + vbNewLine)
                    Next
                End If
        End Select
    End Sub
End Class
