﻿Public Class IndentationDialog

    Private Sub IndentationDialog_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        indentTwoFullWidth.Checked = Indentation.settings.isFullWidth
        indentFourSpace.Checked = Not Indentation.settings.isFullWidth
    End Sub

    Private Sub indentTwoFullWidth_CheckedChanged(sender As Object, e As EventArgs) Handles indentTwoFullWidth.CheckedChanged
        changeSetting()
    End Sub

    Private Sub indentFourSpace_CheckedChanged(sender As Object, e As EventArgs) Handles indentFourSpace.CheckedChanged
        changeSetting()
    End Sub

    ''' <summary>
    ''' Change the settings for this plugin.
    ''' </summary>
    Sub changeSetting()
        Indentation.settings.isFullWidth = indentTwoFullWidth.Checked
    End Sub

End Class