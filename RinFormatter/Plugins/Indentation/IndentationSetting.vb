﻿Public Class IndentationSetting
    Inherits RinFormatterPlugin.Setting

    Private _isFullWidth As Boolean = True

    ''' <summary>
    ''' Determines whether to use two full-width spaces or four half-width spaces for indentation.
    ''' </summary>
    ''' <returns><c>True</c> if to use two full-width spaces.</returns>
    Property isFullWidth As Boolean
        Get
            Return _isFullWidth
        End Get
        Set(value As Boolean)
            _isFullWidth = value
        End Set
    End Property

End Class
