﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class IndentationDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.indentFourSpace = New System.Windows.Forms.RadioButton()
        Me.indentTwoFullWidth = New System.Windows.Forms.RadioButton()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.indentFourSpace)
        Me.GroupBox1.Controls.Add(Me.indentTwoFullWidth)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(171, 67)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Indentation"
        '
        'indentFourSpace
        '
        Me.indentFourSpace.AutoSize = True
        Me.indentFourSpace.Location = New System.Drawing.Point(6, 42)
        Me.indentFourSpace.Name = "indentFourSpace"
        Me.indentFourSpace.Size = New System.Drawing.Size(90, 17)
        Me.indentFourSpace.TabIndex = 1
        Me.indentFourSpace.TabStop = True
        Me.indentFourSpace.Text = "RadioButton2"
        Me.indentFourSpace.UseVisualStyleBackColor = True
        '
        'indentTwoFullWidth
        '
        Me.indentTwoFullWidth.AutoSize = True
        Me.indentTwoFullWidth.Location = New System.Drawing.Point(6, 19)
        Me.indentTwoFullWidth.Name = "indentTwoFullWidth"
        Me.indentTwoFullWidth.Size = New System.Drawing.Size(90, 17)
        Me.indentTwoFullWidth.TabIndex = 0
        Me.indentTwoFullWidth.TabStop = True
        Me.indentTwoFullWidth.Text = "RadioButton1"
        Me.indentTwoFullWidth.UseVisualStyleBackColor = True
        '
        'IndentationDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(195, 91)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "IndentationDialog"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "IndentationDialog"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents indentFourSpace As System.Windows.Forms.RadioButton
    Friend WithEvents indentTwoFullWidth As System.Windows.Forms.RadioButton
End Class
