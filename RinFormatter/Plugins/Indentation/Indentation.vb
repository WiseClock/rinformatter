﻿Public Class Indentation
    Implements RinFormatterPlugin.IPlugin

    ''' <summary>
    ''' The settings of this plugin.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Friend Shared settings As IndentationSetting = New IndentationSetting

    Public Function convert(line As String) As String Implements RinFormatterPlugin.IPlugin.convert
        Return IIf(settings.isFullWidth, "　　" + line, "    " + line)
    End Function

    Public Function convert(lines() As String) As String() Implements RinFormatterPlugin.IPlugin.convert
        Return Nothing
    End Function

    Public ReadOnly Property getPluginName As String Implements RinFormatterPlugin.IPlugin.getPluginName
        Get
            Return "Indentation"
        End Get
    End Property

    Public ReadOnly Property getPluginId As String Implements RinFormatterPlugin.IPlugin.getPluginId
        Get
            Return "com.carlkuang.indentation"
        End Get
    End Property

    Public ReadOnly Property getPluginAuthor As String Implements RinFormatterPlugin.IPlugin.getPluginAuthor
        Get
            Return "Carl Kuang"
        End Get
    End Property

    Public ReadOnly Property getPluginVersion As String Implements RinFormatterPlugin.IPlugin.getPluginVersion
        Get
            Return "1.0"
        End Get
    End Property

    Public ReadOnly Property hasDialogue As Boolean Implements RinFormatterPlugin.IPlugin.hasDialogue
        Get
            Return True
        End Get
    End Property

    Public Sub initialize(settings As RinFormatterPlugin.Setting) Implements RinFormatterPlugin.IPlugin.initialize
        Indentation.settings = settings
    End Sub

    Public ReadOnly Property isLineByLine As Boolean Implements RinFormatterPlugin.IPlugin.isLineByLine
        Get
            Return True
        End Get
    End Property

    Public Function getSettings() As RinFormatterPlugin.Setting Implements RinFormatterPlugin.IPlugin.getSettings
        Return settings
    End Function

    Public Sub showDialogue() Implements RinFormatterPlugin.IPlugin.showDialogue
        'shows the configuration dialogue.
        IndentationDialog.ShowDialog()
    End Sub
End Class
